package org.yesbee.demo;

public class Student {
	
	private String fullName;
	private String lastName;
	private String eMail;
	
	public Student(String fullName, String lastName, String eMail) {
		super();
		this.fullName = fullName;
		this.lastName = lastName;
		this.eMail = eMail;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	
	
}
