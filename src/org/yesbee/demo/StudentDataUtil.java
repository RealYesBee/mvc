package org.yesbee.demo;

import java.util.ArrayList;
import java.util.List;

public class StudentDataUtil {
	
	public static List<Student> getStudents(){
		
		List<Student> ls = new ArrayList<Student>();
		ls.add(new Student("Sagar","Bathini","sb@gmail.com"));
		ls.add(new Student("Vijay","Mali","vm@gmail.com"));
		ls.add(new Student("Harish","Beduru","hb@gmail.com"));
		
		return ls;
		
	}

}
